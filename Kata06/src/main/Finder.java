package main;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Finder {
	
	public ArrayList<String> read(String filename) throws FileNotFoundException{
		ArrayList<String> array = new ArrayList<String>();
		String path = "src/textFiles/" + filename;
		FileInputStream fstream = new FileInputStream(path);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
	
		String strLine;
	
		try {
			while ((strLine = br.readLine()) != null)   {
				array.add(strLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public ArrayList changeToNumbers(ArrayList<String> array){
		Mapper m = new Mapper();
		m.putValues();
		ArrayList resultArray = new ArrayList();
		
		
		for (String element : array) {
			int sum = 1;
			String fixedWord = element + " ";
			String[] tempArray = fixedWord.split("");
			
			for (int i= 0; i<tempArray.length-1; i++){
				int num = m.hmap.get(tempArray[i]);
				sum *= num;
			}
			resultArray.add(sum);
		}
		return resultArray;
	}
	
	public void find(ArrayList<Integer> inputNum, ArrayList<Integer> dictionaryNum,
			ArrayList<String> inputStr, ArrayList<String> dictionrayStr){
		ArrayList<String> finalList = new ArrayList<String>();
		int i = 0;
		for (int first_element : inputNum){
			String sentence = "";
			int j = 0;
			i++;
			for(int second_element : dictionaryNum){
				if(first_element == second_element){
					sentence += dictionrayStr.get(j);
					sentence += " ";
					
				}
				j++;
			}
			System.out.println(sentence);
		}
		
	}
	
	
}
