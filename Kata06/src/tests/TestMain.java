package tests;
import main.Finder;
import main.Main;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;


public class TestMain {

	
	@Test
	public void testInput() throws FileNotFoundException{
		Main main = new Main();
		Finder f = new Finder();
		ArrayList<String> arrayTest = new ArrayList<String>();
		arrayTest.add("kinship ");
		arrayTest.add("enlist ");
		arrayTest.add("boaster ");
		arrayTest.add("fresher ");
		arrayTest.add("sinks ");
		arrayTest.add("knits ");	
		arrayTest.add("rots ");

		ArrayList array = f.read("inputText.txt");
		Assert.assertEquals(arrayTest, array);
	}
	
	@Test
	public void testIfNull() throws FileNotFoundException{
		Main main = new Main();
		Finder f = new Finder();
		ArrayList array = f.read("inputText.txt");
		ArrayList arrayNumber = f.changeToNumbers(array);
		Assert.assertNotEquals(null, arrayNumber);
	}
	
}
